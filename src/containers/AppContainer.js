import React from "react";
import { Provider } from "react-redux";
import store, { history } from "../store/configureStore";
import { ConnectedRouter } from "connected-react-router";
import AppRoutes from "./AppRoutes";

const AppContainer = () => {
  return (
    <Provider store={store}>
      <ConnectedRouter history={history}>
        <AppRoutes />
      </ConnectedRouter>
    </Provider>
  );
};

export default AppContainer;
