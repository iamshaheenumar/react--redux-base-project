import React from "react";
import { Route, Switch } from "react-router";

import appUrls from "../config/appUrls";

const AppRoutes = () => {
  return (
    <Switch>
      <Route exact path={appUrls.HOME} render={() => <div>HOME</div>} />
      <Route exact path={appUrls.LOGIN} render={() => <div>LOGIN</div>} />
      <Route render={() => <div>Miss</div>} />
    </Switch>
  );
};

export default AppRoutes;
